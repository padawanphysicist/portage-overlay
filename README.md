padawanphysicist's portage overlay
==================================

Drop in a gentoo `repos.conf` file:
```
$ sudo tee /etc/portage/repos.conf/padawanphysicist.conf << EOF
[padawanphysicist]
location = /usr/local/portage
sync-type = git
sync-uri = https://gitlab.com/padawanphysicist/portage-overlay
auto-sync = yes
EOF
```
Sync the overlay
```
$ sudo emaint sync -r padawanphysicist
```
